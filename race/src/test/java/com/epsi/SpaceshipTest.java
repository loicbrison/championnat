package com.epsi;

import org.junit.Assert;
import org.junit.Test;



public class SpaceshipTest {

    @Test
    public void testRequiredTimeToMove (){
        // name , warm speed km/h , warm distance, nominal speed km/h
        Spaceship instance = new Spaceship("test", 10, 1, 20);
        int actualRequiredTimeSeconds = instance.moveforward(10); //on 10 km
        Assert.assertEquals(1980,actualRequiredTimeSeconds);
    }
}
