package com.epsi;

//import com.epsi.race.EpsiZone;

public class Championship{
    public static void main(String[]args){
        EpsiZone simpleZone = new EpsiZone("EPSIZone1");
        simpleZone.setLapDistance(20);//km
        simpleZone.setNumberofLaps(10);
        //name,warmspeedkm/h,warmdistance,nominalspeedkm/hSpaceshipv=newSpaceship(”V”,150,5,165);
        Spaceship v = new Spaceship ("V", 150, 5, 165) ;
        simpleZone.enrollSpaceship(v);
        //name,warmspeedkm/h,warmdistance,nominalspeedkm/h
        Spaceship silverhand= new Spaceship("Silverhand", 155, 8, 165);
        simpleZone.enrollSpaceship(silverhand);
        String str="Welcome to %s. The distance of the lapis %d.";
        System.out.println(
            String.format(str,
                simpleZone.getZone(),
                simpleZone.getDistance()));
        simpleZone.start();
        String win="The winneris %s.";
        System.out.println(String.format(win, simpleZone.getWinner().getName()));
    }
}
