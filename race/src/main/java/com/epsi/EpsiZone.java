package com.epsi;

import java.util.ArrayList;

public class EpsiZone {
    
    private String name;
    private int lapDistance; //Km/h
    private int lapNumber; //Km
    private ArrayList<Spaceship> concurrents = new ArrayList<>();
    private Spaceship winner;

    public EpsiZone(String name){
        this.name = name;
    }

    public String getZone() {
        return this.name;
    }

    public int getDistance() {
        return this.lapDistance*this.lapNumber;
    }

    public void setLapDistance(int lapDistance) {
        this.lapDistance = lapDistance;
    }

    public void setNumberofLaps(int lapNumber) {
        this.lapNumber = lapNumber;
    }

    public void enrollSpaceship(Spaceship sp) {
        this.concurrents.add(sp);
    }

    public Spaceship getWinner(){
        return this.winner;
    }

    public void start(){
        Spaceship winner = this.concurrents.get(0) ;
        int min = this.concurrents.get(0).moveforward(this.getDistance());
        int tmp;

        for(Spaceship sp : this.concurrents){
            tmp = sp.moveforward(this.getDistance());
            if(tmp<min){
                min = tmp;
                winner = sp;
            }
        }
        this.winner = winner;
    }

}
