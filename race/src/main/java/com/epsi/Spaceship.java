package com.epsi;

public class Spaceship {
    
    private String name;
    private int warmSpeed; //Km/h
    private int warmdistance; //Km
    private int nominalSpeed; //Km/h

    public Spaceship(String name, int warmSpeed, int warmdistance, int nominalSpeed){
        this.name = name;
        this.warmSpeed = warmSpeed; //Km/h
        this.warmdistance = warmdistance;  //Km
        this.nominalSpeed = nominalSpeed; //Km/h
    }

    public String getName() { 
        return this.name;
    }

    public int moveforward(int distance){ 
        // distance km
        // retourne le temps pour parcourir la distance donné en paramètre 
        double warmTime = (double)this.warmdistance / (double)this.warmSpeed;
        double leftTime = (double)(distance-this.warmdistance) / (double)this.nominalSpeed;
        double res = (leftTime+warmTime)*3600;
        return (int) res;
    }

}
